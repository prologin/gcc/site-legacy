from django.core.management import BaseCommand
from gcc.models import EventWish, ApplicantStatusTypes
import csv

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('event_id', type=int)
        parser.add_argument('out_path', type=str)

    def handle(self, event_id, out_path, *args, **options):
        wishes = EventWish.objects.filter(
            event_id=event_id,
            status__in=(ApplicantStatusTypes.accepted.value, ApplicantStatusTypes.confirmed.value),
        ).values_list(
            'id',
            'applicant__user__email',
            'applicant__user__first_name',
            'applicant__user__last_name',
        )

        with open(out_path, 'wt') as f:
            w = csv.writer(f)
            w.writerow(('eventwish_id', 'email', 'first_name', 'last_name'))
            for wish in wishes:
                w.writerow(wish)

        print(f"CSV saved to {out_path}")
